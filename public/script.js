var globalGameId = null;
var playerHealth;
var playerCoins;

Vue.component('mission', {
    props: ['mission'],
    template: 
    '<div class="card" v-on:click="tryMission">' + 
        '<h4 class="h4">{{ mission.message }}</h4>' + 
        '<ul class="mission__item-list">' + 
            '<li class="mission__item">Expires in: {{ mission.expiresIn }} days</li>' +
            '<li class="mission__item">Probability of success: {{ mission.probability }}</li>' +
            '<li class="mission__item text-accent">Reward: {{ mission.reward }} coins</li>' +
        '</ul>' +
    '</div>',
    methods: {
        tryMission: function(event) {
            const currentMission = $(event.currentTarget);
            $missionId = this.$vnode.key;

            axios
                .post('https://dragonsofmugloar.com/api/v2/' + globalGameId + '/solve/' + $missionId)
                .then(response => (
                    response.data.success ? currentMission.addClass('is-success') : currentMission.addClass('is-failure'),
                    $('.player-health').text(response.data.lives),
                    $('.player-coins').text(response.data.gold),
                    $('.player-score').text(response.data.score),
                    $('.mission-turns').text(response.data.turn),
                    response.data.lives === 0 ? alert("You have perished! Reload the game to try again") : '' 
                ))
                .catch(error => (
                    console.log(error),
                    alert('You are out of turns or the mission has expired')
                ))
        }
    }
})

Vue.component('store-item', {
    props: ['item'],
    template: 
    '<div class="card" v-on:click="tryPurchase">' + 
        '<h4 class="h4">{{ item.name }}</h4> <p class="store-item__cost text-accent">{{ item.cost }} coins</span></p>' +
    '</div>',
    methods: {
        tryPurchase: function(event) {
            const currentItem = $(event.currentTarget);
            $itemId = this.$vnode.key;
            axios
            .post('https://dragonsofmugloar.com/api/v2/' + globalGameId + '/shop/buy/' + $itemId)
            .then(response => {
                if(response.data.shoppingSuccess) {
                    alert("Purchase was successful")
                    $('.player-health').text(response.data.lives),
                    $('.player-coins').text(response.data.gold),
                    $('.mission-turns').text(response.data.turn)
                } else {
                    alert("Purchase unsuccessful. Check your resources.")
                }
            })
        }
    }
})

var appContainer = new Vue({
    el: '.app__container',
    data () {
        return {
            info: null,
            gameId: null,
            formattedGameId: null,
            storeLoaded: false,
            gameLoaded: false,

            missions: [],
            store: [],
            playerHealth: playerHealth,
            playerCoins: playerCoins,
            reputation: []
        }
    },
    methods: {
        startGame: function(event) {
            axios
            .post('https://dragonsofmugloar.com/api/v2/game/start')
            .then(response => (
                this.info = response.data, 
                this.gameLoaded = response.status === 200 ? true : false,
                this.formattedGameId = 'Game id: ' + response.data.gameId, 
                this.gameId = response.data.gameId,
                globalGameId = response.data.gameId
                ))
            .catch(error => (
                console.log(error),
                alert('We were not able to process your request at this time. Please try again later.')
            ))
        },
        getMissions: function(event) {
            $gameId = this.gameId
            $('.app__missions').removeClass('is-hidden');
            if (!$('.app__store').hasClass('is-hidden')) {
                $('.app__store').addClass('is-hidden')
            };
            if (!$('.app__status').hasClass('is-hidden')) {
                $('.app__status').addClass('is-hidden')
            }
            axios
            .get('https://dragonsofmugloar.com/api/v2/'+ $gameId +'/messages')
            .then(response => (
                this.missions = response.data
            ))
            .catch(error => (
                console.log(error),
                alert('Missions could not be loaded at this time, please try again later')
            ))
            
        },
        getStore: function(event) {
            $gameId = this.gameId
            $('.app__store').removeClass('is-hidden');
            if (!$('.app__missions').hasClass('is-hidden')) {
                $('.app__missions').addClass('is-hidden')
            }
            if (!$('.app__status').hasClass('is-hidden')) {
                $('.app__status').addClass('is-hidden')
            }
            if (!this.storeLoaded) {
                axios
                .get('https://dragonsofmugloar.com/api/v2/' + $gameId + '/shop')
                .then(response => (
                    this.storeLoaded = response.status === 200 ? true : false,
                    this.store = response.data
                ))
                .catch(error => (
                    console.log(error),
                    alert('Store items could not be loaded at this time, please try again later')
                ))
            }
        },
        getStatus: function(event) {
            $gameId = this.gameId
            $('.app__status').removeClass('is-hidden');
            if (!$('.app__missions').hasClass('is-hidden')) {
                $('.app__missions').addClass('is-hidden')
            }
            if (!$('.app__store').hasClass('is-hidden')) {
                $('.app__store').addClass('is-hidden')
            }
            axios
                .post('https://dragonsofmugloar.com/api/v2/'+ $gameId +'/investigate/reputation')
                .then(response => (
                    this.repLoaded = response.status = 200 ? true : false,
                    this.reputation = response.data
                ))
                .catch(error => (
                    console.log(error),
                    alert('Reputation data could not be loaded at this time, please try again later')
                ))
        },
    }
})